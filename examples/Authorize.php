<?php
namespace app\api\controller;


use app\api\services\helper\OauthHelper;

/**
 * 用户认证
 *
 * Class Oauth
 * @package app\api\controller
 */
class Authorize
{
    /**
     * 获取token
     */
    public function token()
    {
        try {
            $post = $_POST;
            $header = self::getHeaderData($_SERVER);
            if (empty($state = $post['state']) || empty($post['redirect_uri'])) throw new \Exception('参数错误');
            //设置state缓存
            //Cache::store('redis')->set($state,1);
            $token = OauthHelper::accessToken($header, $post);
            echo (['code' => 0, 'msg' => 'ok', 'data' => $token]);
        } catch (\Exception $exception) {
            echo (['code' => 1, 'msg' => $exception->getMessage()]);
        }
    }

    private function getHeaderData($server)
    {
        foreach ($server as $key => $val) {
            if (0 === strpos($key, 'HTTP_')) {
                $key          = str_replace('_', '-', strtolower(substr($key, 5)));
                $header[$key] = $val;
            }
        }
        return $header ?? [];
    }

    /**
     * 请求回调
     */
    public function notify()
    {
        try {
            $params = $_GET;
            if (empty($params['state'])) {
                throw new \Exception('验证失败， 参数错误');
            }
            //$cache = Cache::store('redis')->get($params['state']);
            if (empty($cache)) {
                throw new \Exception('验证失败！');
            }
            echo (['code' => 0, 'msg' => 'ok']);
        } catch (\Exception $exception) {
            echo (['code' => 0, 'msg' => $exception->getMessage()]);
        }

    }

}
