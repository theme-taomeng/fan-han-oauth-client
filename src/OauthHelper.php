<?php


namespace app\api\services\helper;


/**
 * oauth认证辅助类
 *
 * Class OauthHelper
 * @package app\api\services\helper
 */
class OauthHelper
{
    const SERVER_HOST = "https://oauth.fhdianshang.com";
    const LOCAL_HOST = "https://sign.fhdianshang.com";

    const GET_AUTH_CODE_URL = self::SERVER_HOST."/api/authorize/code";
    const GET_ACCESS_TOKEN_URL = self::SERVER_HOST."/api/authorize/token";

    const REDIRECT_URI = self::LOCAL_HOST.'/api/authorize/notify';//成功授权后的回调地址

    /**
     * 获取token
     *
     * @param array $paramsHeader
     * @param array $params
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function accessToken(array $header, array $params)
    {
        if (empty($header['authorization'])) throw new \Exception('Authorization不能为空');
        $paramsHeader = $header['authorization'];
        //反解锁认证数据
        $headerData = OauthHelper::getHeaderAuthorization($paramsHeader);
        $headerData = ['appid' => $headerData[0] ?? '', 'app_secret' => $headerData[1] ?? ''];
        if (empty($headerData['appid']) || empty($headerData['app_secret'])) throw new \Exception('用户key、密钥不能为空');
        //获取认证code
        $codeResult =  OauthHelper::getCode([
            'appid' => $headerData['appid'],
            'state' => $params['state'] ?? '',
            'redirect_uri' => $params['redirect_uri'] ?? '',
        ]);

        if (empty($codeResult['data']['code'])) throw new \Exception($codeResult['msg'] ?? '获取code失败');
        //code换取token
        $token = OauthHelper::getToken([
            'code' => $codeResult['data']['code'],
            'appid' => $headerData['appid'],
            'app_secret' => $headerData['app_secret'],
        ]);

        return $token;
    }

    /**
     * 获取code
     *
     * @return mixed
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function getCode($app_param=array())
    {
        $data['state'] =  $app_param['state'] ?? '';
        $data['appid'] =  $app_param['appid'] ?? '';
        $data['redirect_uri'] =  $app_param['redirect_uri'] ?? '';
        $params = [
            'authorized' => '登录'
        ];
        $authorze_url = self::getAuthorizeUrl($data);
        return self::post($authorze_url, $params);
    }

    /**
     * 获取Header认证信息
     *
     * @param string $authorization
     * @return false|string
     */
    public static function getHeaderAuthorization(string $authorization) : array
    {
        $str = substr($authorization, 6);
        $params = base64_decode($str);
        return explode(':', $params);
    }

    /**
     * 生成code请求连接
     *
     * @param array $app_param
     * @return string
     */
    private static function getAuthorizeUrl($app_param=array())
    {
        $params = array(
            'response_type'=>'code',
            'client_id'=>isset($app_param['appid'])?$app_param['appid']:self::APP_ID,
            'redirect_uri'=>isset($app_param['redirect_uri'])?$app_param['redirect_uri']:self::REDIRECT_URI,
            'state'=>$app_param['state'],
        );
        $authorze_url = self::GET_AUTH_CODE_URL.'?'.http_build_query($params);
        return $authorze_url;
    }

    public static function getToken($app_param=array()){
        $params = array(
            'grant_type'=>'authorization_code',
            'client_id'=>isset($app_param['appid'])?$app_param['appid']:self::APP_ID,
            'redirect_uri'=>isset($app_param['redirect_uri'])?$app_param['redirect_uri']:self::REDIRECT_URI,
            'client_secret'=>isset($app_param['app_secret'])?$app_param['app_secret']:self::APP_SECRET,
            'code'=>$app_param['code'],
        );
        $response = self::post(self::GET_ACCESS_TOKEN_URL,$params);
        return $response;
    }

    public static function getApiData($app_param=array()){

        $params = array(
            'access_token'=>$app_param['access_token'],
        );
        $openid_url = self::GET_RESOURCE_URL.'?'.http_build_query($params);
        //   echo $openid_url;
        $str  = file_get_contents($openid_url);
        $data = json_decode($str,true);
        return $data;
    }

    /***
     * @param $url
     * @param array $header_options
     * @return mixed
     */
    static function get($url,array $header_options = array())
    {
        $ch = curl_init();
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1, //返回原生的（Raw）输出
            //            CURLOPT_HEADER => 0,
            //            CURLOPT_TIMEOUT => 120, //超时时间
            //            CURLOPT_FOLLOWLOCATION => 1, //是否允许被抓取的链接跳转
            CURLOPT_ENCODING=>'gzip,deflate',
            CURLOPT_HTTPHEADER => $header_options,
        );
        if (strpos($url,"https")!=false) {
            $curl_options[CURLOPT_SSL_VERIFYPEER] = false; // 对认证证书来源的检查
        }
        curl_setopt_array($ch, $curl_options);
        $res = curl_exec($ch);
        $data = json_decode($res,true);
        if(json_last_error() != JSON_ERROR_NONE){
            $data = $res;
        }
        curl_close($ch);
        return $data;
    }
    /**
     * post 请求
     * @param $url 请求url
     * @param array $param  post参数
     * @param array $header 头部信息
     * @param bool $login   是否登陆
     * @param int $ssl      启用ssl
     * @param int $log      是否记录日志
     * @param string $format返回数据格式
     * @return mixed
     */
    static function post($url, array $param = array(), array $header = array())
    {
        $ch = curl_init();
        $post_param = array();
        if (is_array($param)) {
            $post_param = http_build_query($param);
        } else if (is_string($param)) { //json字符串
            $post_param = $param;
        }
        $header_options =  $header;
        $curl_options = array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => 1, //返回原生的（Raw）输出
            CURLOPT_HEADER => 0,
            CURLOPT_TIMEOUT => 120, //超时时间
            CURLOPT_FOLLOWLOCATION => 1, //是否允许被抓取的链接跳转
            CURLOPT_HTTPHEADER => $header_options,
            CURLOPT_POST => 1, //POST
            CURLOPT_POSTFIELDS => $post_param, //post数据
            CURLOPT_ENCODING=>'gzip,deflate'
        );

        if (strpos($url,"https")!==false) {
            $curl_options[CURLOPT_SSL_VERIFYPEER] = false; // 对认证证书来源的检查
        }
        curl_setopt_array($ch, $curl_options);
        $res = curl_exec($ch);
        $data = json_decode($res, true);

        if(json_last_error() != JSON_ERROR_NONE){
            $data = $res;
        }
        curl_close($ch);
        return $data;
    }
}